package com.example.examen3tuno

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.examen3tuno.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding: FragmentFirstBinding
    private lateinit var adapter: AdaptadorMensajes

    private val myViewModel by activityViewModels<MyViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(requireContext())
        adapter = AdaptadorMensajes()
        binding.recyclerview.layoutManager = layoutManager

        //todo funcion para asignar nuestro adaptador como adapter del RecyclerView
        //binding.recyclerview.adapter = adapter

        myViewModel.mensajes.observe(viewLifecycleOwner) {
            adapter.update(it)
        }

        myViewModel.getAllMessages()

        binding.botonEnviar.setOnClickListener { enviarMensaje() }

        // incorpora al recyclerview la función para eliminar arrastrando
        val arrastraParaEliminar = ItemTouchHelper(simpleCallback)
        arrastraParaEliminar.attachToRecyclerView(binding.recyclerview)
    }

    private fun enviarMensaje() {
        val campoTexto = binding.campotexto
        val tarea = campoTexto.text.toString()

        val textoFecha = "Desconocida"

        val mensaje = Mensaje(tarea, textoFecha)
        adapter.aniadirTarea(mensaje)
    }

    /**
     * Función listener que se activa cuando arrastramos un item del listado hacia un lado
     */
    private val simpleCallback = object :
        ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.START or ItemTouchHelper.END
        ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            // Función del adaptador para eliminar mensajes
            adapter.eliminarTarea(viewHolder.adapterPosition)
        }
    }

}