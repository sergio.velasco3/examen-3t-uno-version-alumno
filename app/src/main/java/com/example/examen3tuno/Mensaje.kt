package com.example.examen3tuno


/**
 * Created by sergi on 28/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

data class Mensaje(
    var mensaje: String,
    var fecha: String
)
