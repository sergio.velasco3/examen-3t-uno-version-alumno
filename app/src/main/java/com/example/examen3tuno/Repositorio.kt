package com.example.examen3tuno

import com.example.examen3tuno.Mensaje

class Repositorio {

    fun listaInicial(): MutableList<Mensaje> {
        val mensaje1 = Mensaje("Fin de curso", "2023/06/22 23:59:33")
        val mensaje2 = Mensaje("Jugar partido", "2023/05/01 10:00:11")
        val mensaje3 = Mensaje("Estudiar para el examen", "2023/04/25 16:00:22")

        return mutableListOf(mensaje1, mensaje2, mensaje3)
    }
}