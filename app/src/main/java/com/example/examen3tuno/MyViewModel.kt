package com.example.examen3tuno

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.examen3tuno.Repositorio
import com.example.examen3tuno.Mensaje

class MyViewModel: ViewModel() {

    private val repositorio = Repositorio()

     val mensajes = MutableLiveData<List<Mensaje>>()

    fun getAllMessages() {
        mensajes.postValue(repositorio.listaInicial())
    }
}