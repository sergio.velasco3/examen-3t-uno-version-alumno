package com.example.examen3tuno

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.examen3tuno.databinding.VistaCeldaBinding


/**
 * Created by sergi on 28/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class AdaptadorMensajes  {

    inner class MyHolder(val binding: VistaCeldaBinding) : ViewHolder(binding.root)

    private val listado = ArrayList<Mensaje>()

    fun aniadirTarea(mensaje: Mensaje) {
        // todo completar función
        listado.add(mensaje)
    }

    fun eliminarTarea(position: Int) {
        //todo completar funcion

        // todo quita estos comentarios a las funciones de abajo para actualizar el adaptador, están comentadas porque dan error sin la clase no hereda de donde debe heredar
        //notifyItemRemoved(position)
        //notifyItemRangeChanged(position, listado.size)
    }

    fun update(lista: List<Mensaje>) {
        listado.clear()
        listado.addAll(lista)
        // todo quita estos comentarios a las funciones de abajo para actualizar el adaptador, están comentadas porque dan error sin la clase no hereda de donde debe heredar
        //notifyItemRangeChanged(0, listado.size)
    }
}