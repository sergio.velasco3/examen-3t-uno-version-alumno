package com.example.examen3tuno

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.examen3tuno.databinding.FragmentSecondBinding
import java.util.Calendar
import java.util.concurrent.TimeUnit

class SecondFragment : Fragment() {

    private lateinit var binding: FragmentSecondBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this).load(R.drawable.cat).into(binding.ivGif)

        hacerCuentaAtras()
    }

    private fun hacerCuentaAtras() {

        val fechaActual = Calendar.getInstance()
        val fechaFinal = Calendar.getInstance()
        fechaFinal.set(2023, 3, 26, 14, 30)

        val diferencia = fechaFinal.timeInMillis - fechaActual.timeInMillis

        val counter = object : CountDownTimer(diferencia, 1000) {
            override fun onTick(p0: Long) {
                var millisHastaTerminar = p0
                val days: Long = TimeUnit.MILLISECONDS.toDays(millisHastaTerminar)
                millisHastaTerminar -= TimeUnit.DAYS.toMillis(days)

                val hours: Long = TimeUnit.MILLISECONDS.toHours(millisHastaTerminar)
                millisHastaTerminar -= TimeUnit.HOURS.toMillis(hours)

                val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(millisHastaTerminar)
                millisHastaTerminar -= TimeUnit.MINUTES.toMillis(minutes)

                val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(millisHastaTerminar)

                binding.tvCuentaAtras.text =
                    "$days día $hours horas $minutes minutos $seconds segundos"
            }

            override fun onFinish() {
                binding.tvCuentaAtras.text = "Tiempo terminado"
            }
        }
        counter.start()
    }
}